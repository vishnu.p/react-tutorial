import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Square extends React.Component {
  render() {
    return (
        <button className="square" onClick={() => this.props.onClick(this.props.value)}>
        {this.props.value}
      </button>
    );
  }
}

class Board extends React.Component {
  constructor(props) {
    super(props);
    var array = [];    
    for (var i = 0; i < this.props.count; i++) {
      array.push(i)
    }
    
    this.state = {
      value: array
    };
  }
  
  shift(value) {
    let arr = this.state.value;
    
    let popped = arr.splice(0, 1);
    popped.map((item, i) => {
      arr.push(popped[i])
    })

    this.setState({
      value: arr
    })
  }

  renderSquare(i) {
    return <Square value={i} onClick={this.shift.bind(this)} />
  }
  
  render() {
    return (
      <div className="container">
        <div className="row my-5">
          {
            this.state.value.map((item, i) => {
            return (
              <div className="col" key={i} >
                <div className="card">
                  {this.renderSquare(item)}
                </div>
              </div>
            )
          })
          }
        </div>
      </div>
    );
  }
}


// ========================================

ReactDOM.render(
  <Board count="5" />,
  document.getElementById('root')
);
